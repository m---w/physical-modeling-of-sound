# Physical Modeling of Sound and Material Science


Materials of the university course "Physical Modeling of Sound and Material Science" held between 2019 and 2023 at the University of Music and Performing Arts Graz, Austria.

2019-2023 Marian Weger


This one semester course includes 13 sessions with slides, programming examples, and homework assignments.
Plus 2 sessions for Pd programming introduction and Q&A (without prepared materials).

## Learning Outcomes

Fundamental knowledge in physical modelling of sound generating objects, as well as in the acoustical effects of physical properties (materia, shape and dimensions, boundary conditions, etc.). Students are able to identify the factors of physical processes that are essential for the perceived sound, simplify them if necessary, and synthesize them using digital audio processing methods. 

## Content

The course combines existing basic knowledge in acoustics, digital audio processing and sound synthesis in order to gain a deeper understanding of the physical creation of sound. Physical basics of oscillating systems are considered, starting with the mechanical spring pendulum (and its equivalents such as electric resonant circuit and cavity resonator), up to multidimensional cavity and surface resonance systems. On the basis of concrete examples (simple physical objects, electrical circuits, musical instruments, voice, room acoustics, etc.), common methods of physical modeling such as modal analysis / modal synthesis, digital waveguides and lumped mass-spring networks are derived and put into practice. Building on existing skills in programming with Pure Data (Pd), the theoretical models are implemented as digital synthesis models and individually extended on the basis of specific tasks.
